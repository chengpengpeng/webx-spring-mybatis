<#assign className = table.className>   
<#assign classNameFirstLower = className?uncap_first>   
<#assign classNameLowerCase = className?lower_case>   
package ${basepackage}.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tzg.core.page.PageResult;
import com.tzg.core.page.PaginationQuery;
import com.tzg.service.base.BaseService;

@Service
@Transactional
public class ${className}Service extends BaseService {

	@Autowired
	private ${className}Mapper ${classNameFirstLower}Mapper;	
	   
	@Transactional(readOnly=true)
    public ${className} findById(${table.idColumn.javaType} id) throws Exception {
    	if(id == null){
			throw new Exception("id不能为空");
		}
        return ${classNameFirstLower}Mapper.findById(id);
    }
	
    public void delete(${table.idColumn.javaType} id) throws Exception {
    	if(id == null){
			throw new Exception("id不能为空");
		}
        ${classNameFirstLower}Mapper.deleteById(id);
    }
	
	public void save(${className} ${classNameFirstLower}) throws Exception {	    
		${classNameFirstLower}Mapper.save(${classNameFirstLower});
	}
	
	public void update(${className} ${classNameFirstLower}) throws Exception {	
		if(${classNameFirstLower}.getId() == null){
			throw new Exception("id不能为空");
		}
		${classNameFirstLower}Mapper.update(${classNameFirstLower});
	}	
	
	@Transactional(readOnly=true)
	public PageResult<${className}> findPage(PaginationQuery query) throws Exception {
		PageResult<${className}> result = null;
		try {
			Integer count = ${classNameFirstLower}Mapper.findPageCount(query.getQueryData());
			if (null != count && count.intValue() > 0) {
				int startRecord = (query.getPageIndex() - 1)* query.getRowsPerPage();
				query.addQueryData("startRecord", Integer.toString(startRecord));
				query.addQueryData("endRecord", Integer.toString(query.getRowsPerPage()));
				List<${className}> list = ${classNameFirstLower}Mapper.findPage(query.getQueryData());
				result = new PageResult<${className}>(list,count,query);
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	

	
}

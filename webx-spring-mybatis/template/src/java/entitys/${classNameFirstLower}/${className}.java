<#assign className = table.className>
<#assign classNameLower = className?uncap_first> 
package ${basepackage}.api.entitys.${className}; 

import java.io.Serializable;
import com.tzg.tool.kit.date.DateUtil;

public class ${className} implements Serializable {
	private static final long serialVersionUID = 1L;
	<#list table.columns as column>
    /**
     * ${column.columnAlias!}
     */ 	
	<#if column.isDateTimeColumn>
	private ${column.javaType} ${column.columnNameFirstLower};
	private java.lang.String ${column.columnNameFirstLower}Str;
	<#else>
	private ${column.javaType} ${column.columnNameFirstLower};
	</#if>	
	</#list>

<@generateJavaColumns/>
<@generateJavaOneToMany/>
<@generateJavaManyToOne/>

	
}

<#macro generateConstructor constructor>
	public ${constructor}(){
	}

	public ${constructor}(
	<#list table.compositeIdColumns as column>
		${column.javaType} ${column.columnNameFirstLower}<#if column_has_next>,</#if>
	</#list>		
	){
	<#list table.compositeIdColumns as column>
		<#if column.pk>
		this.${column.columnNameFirstLower} = ${column.columnNameFirstLower};
		</#if>
	</#list>	
	}

</#macro>

<#macro generateJavaColumns>
	<#list table.columns as column>
	<#if column.isDateTimeColumn>
	
	public void set${column.columnName}(${column.javaType} value) {
		this.${column.columnNameFirstLower}Str =DateUtil.getDate(value, "yyyy-MM-dd HH:mm:ss");
		this.${column.columnNameFirstLower} = value;
	}
	
	public ${column.javaType} get${column.columnName}() {
		return this.${column.columnNameFirstLower};
	}
	
	public java.lang.String get${column.columnName}Str() {
		return this.${column.columnNameFirstLower}Str;
	}
	<#else>
	
	public void set${column.columnName}(${column.javaType} value) {
		this.${column.columnNameFirstLower} = value;
	}
	
	public ${column.javaType} get${column.columnName}() {
		return this.${column.columnNameFirstLower};
	}
	</#if>
	</#list>
</#macro>

<#macro generateJavaOneToMany>
	<#list table.exportedKeys.associatedTables?values as foreignKey>
	<#assign fkSqlTable = foreignKey.sqlTable>
	<#assign fkTable    = fkSqlTable.className>
	<#assign fkPojoClass = fkSqlTable.className>
	<#assign fkPojoClassVar = fkPojoClass?uncap_first>
	
	private Set ${fkPojoClassVar}s = new HashSet(0);
	public void set${fkPojoClass}s(Set<${fkPojoClass}> ${fkPojoClassVar}){
		this.${fkPojoClassVar}s = ${fkPojoClassVar};
	}
	
	public Set<${fkPojoClass}> get${fkPojoClass}s() {
		return ${fkPojoClassVar}s;
	}
	</#list>
</#macro>

<#macro generateJavaManyToOne>
	<#list table.importedKeys.associatedTables?values as foreignKey>
	<#assign fkSqlTable = foreignKey.sqlTable>
	<#assign fkTable    = fkSqlTable.className>
	<#assign fkPojoClass = fkSqlTable.className>
	<#assign fkPojoClassVar = fkPojoClass?uncap_first>
	
	private ${fkPojoClass} ${fkPojoClassVar};
	
	public void set${fkPojoClass}(${fkPojoClass} ${fkPojoClassVar}){
		this.${fkPojoClassVar} = ${fkPojoClassVar};
	}
	
	public ${fkPojoClass} get${fkPojoClass}() {
		return ${fkPojoClassVar};
	}
	</#list>
</#macro>

<#assign className = table.className>   
<#assign classNameLower = className?uncap_first>   
package ${basepackage}.backstage.app.dao.entitys.${className};

import ${basepackage}.backstage.app.dao.entitys.base.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface ${className}Mapper extends BaseMapper<${className}, ${table.idColumn.javaType}> {	

}

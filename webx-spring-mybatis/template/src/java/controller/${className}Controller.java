<#include "/custom.include">
<#assign className = table.className>   
<#assign classNameFirstLower = className?uncap_first>   
<#assign classNameLowerCase = className?lower_case>   
<#assign pkJavaType = table.idColumn.javaType>
package ${basepackage}.backstage.home.controller.manager;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tzg.action.BaseController;
import com.tzg.backstage.api.common.page.PageResult;
import com.tzg.backstage.api.common.page.PaginationQuery;
import com.tzg.backstage.app.common.utils.TzgConstant;
import com.tzg.tool.kit.string.StringUtil;

@Controller
@RequestMapping("/${classNameFirstLower}")
public class ${className}Controller extends BaseController {
	private static Logger logger = LoggerFactory.getLogger(${className}Controller.class);
	
	@Autowired
	private ${className}Service ${classNameFirstLower}Service;
	
	@RequestMapping(value="/new")
	public String add() throws Exception {		
		return "/${classNameFirstLower}/new";
	}
	
	@RequestMapping(value="/find")	
	public String find(Integer id, Model model){
		try {	
			${className} ${classNameFirstLower} = ${classNameFirstLower}Service.findById(id);
			model.addAttribute("param", ${classNameFirstLower});
			model.addAttribute(SUCCESS, true);
			return "/${classNameFirstLower}/update";
		} catch (Exception e) {
			commonError(logger, e, "修改跳转异常", model);
			return "/${classNameFirstLower}/update";
		}
	}
	
	@RequestMapping(value="/save")
	@ResponseBody
	public Map<String, Object> save(@ModelAttribute("${classNameFirstLower}") ${className} ${classNameFirstLower},Model model) {		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			${classNameFirstLower}Service.save(${classNameFirstLower});
			model.addAttribute(SUCCESS, true);
			map.put(SUCCESS, true);
            map.put(MESSAGE, TzgConstant.MESSAGE_RECORD_SAVE_SUCESS);
            
		} catch (Exception e) {
			commonError(logger, e, "添加异常",map); 
		}
		return map;
	}
	
	@RequestMapping(value="/update")
	@ResponseBody
	public Map<String, Object> update(@ModelAttribute("${classNameFirstLower}") ${className} ${classNameFirstLower},Model model) throws Exception {		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			${classNameFirstLower}Service.update(${classNameFirstLower});
			model.addAttribute(SUCCESS, true);
			map.put(SUCCESS, true);
            map.put(MESSAGE, TzgConstant.MESSAGE_RECORD_UPDATE_SUCESS);
		} catch (Exception e) {
			commonError(logger, e,"修改异常",map); 
		}
		return map;
	}
	
	@RequestMapping(value="/delete")
	@ResponseBody
	public Map<String, Object> delete(Integer id,Model model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			${classNameFirstLower}Service.delete(id);
			map.put(SUCCESS, true);
            map.put(MESSAGE, TzgConstant.MESSAGE_RECORD_UPDATE_SUCESS);
		} catch (Exception e) {
			commonError(logger, e,"删除异常",map); 
		}
		return map;
	}
	
	@RequestMapping(value="/list")
	public String list(PaginationQuery query,Model model) throws Exception{		
		PageResult<${className}> pageList = ${classNameFirstLower}Service.findPage(query);
		model.addAttribute("result", pageList);
		model.addAttribute("query", query.getQueryData());
		return "/${classNameFirstLower}/list";
	}
	
}


